require ipmimanager,1.0.0
require essioc

epicsEnvSet("ENGINEER","Wojciech")
epicsEnvSet("LOCATION","DMCS")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")



# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 1)


# crate IP address
epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "172.30.240.19")
epicsEnvSet("TELNET_PORT", "23")

# 1=enable/0=disable archiver
epicsEnvSet("ARCHIVER", "1")
# archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

# panels compatibility version
epicsEnvSet("PANEL_VER", "1.0.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode 
epicsEnvSet("NAME_MODE", 1)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "ipmiManager/Db/auto-gen-db")

############################
# connect to specific MCH
############################

######################
# for NAME_MODE=1
######################

epicsEnvSet("P", "TD-D218:")

epicsEnvSet("MTCA_PREF", "$(P):")
epicsEnvSet("IOC_PREF", "$(P):")

epicsEnvSet("SLOT1_MODULE", "AMC")
epicsEnvSet("SLOT1_IDX", "1")
epicsEnvSet("SLOT2_MODULE", "AMC")
epicsEnvSet("SLOT2_IDX", "2")
epicsEnvSet("SLOT3_MODULE", "CPU")
epicsEnvSet("SLOT3_IDX", "1")
epicsEnvSet("SLOT4_MODULE", "EVR")
epicsEnvSet("SLOT4_IDX", "1")
epicsEnvSet("SLOT5_MODULE", "AMC")
epicsEnvSet("SLOT5_IDX", "3")
epicsEnvSet("SLOT6_MODULE", "CPU")
epicsEnvSet("SLOT6_IDX", "2")
epicsEnvSet("SLOT7_MODULE", "EVG")
epicsEnvSet("SLOT7_IDX", "1")
epicsEnvSet("SLOT8_MODULE", "AMC")
epicsEnvSet("SLOT8_IDX", "4")
epicsEnvSet("SLOT9_MODULE", "AMC")
epicsEnvSet("SLOT9_IDX", "5")
epicsEnvSet("SLOT24_MODULE", "RTM")
epicsEnvSet("SLOT24_IDX", "1")
epicsEnvSet("SLOT32_MODULE", "PM")
epicsEnvSet("SLOT32_IDX", "1")
epicsEnvSet("SLOT33_MODULE", "PM")
epicsEnvSet("SLOT33_IDX", "2")
epicsEnvSet("SLOT48_MODULE", "CU")
epicsEnvSet("SLOT48_IDX", "1")
epicsEnvSet("SLOT49_MODULE", "CU")
epicsEnvSet("SLOT49_IDX", "2")

epicsEnvSet("CHASSIS_CONFIG","SLOT1_MODULE=$(SLOT1_MODULE)", "SLOT1_IDX=$(SLOT1_IDX)", "SLOT2_MODULE=$(SLOT2_MODULE)", "SLOT2_IDX=$(SLOT2_IDX)", "SLOT3_MODULE=$(SLOT3_MODULE)","SLOT3_IDX=$(SLOT3_IDX)", "SLOT4_MODULE=$(SLOT4_MODULE)", "SLOT4_IDX=$(SLOT4_IDX)", "SLOT5_MODULE=$(SLOT5_MODULE)", "SLOT5_IDX=$(SLOT5_IDX)", "SLOT6_MODULE=$(SLOT6_MODULE)", "SLOT6_IDX=$(SLOT6_IDX)","SLOT7_MODULE=$(SLOT7_MODULE)", "SLOT7_IDX=$(SLOT7_IDX)", "SLOT8_MODULE=$(SLOT8_MODULE)", "SLOT8_IDX=$(SLOT8_IDX)", "SLOT9_MODULE=$(SLOT9_MODULE)", "SLOT9_IDX=$(SLOT9_IDX)", "SLOT24_MODULE=$(SLOT24_MODULE)", "SLOT24_IDX=$(SLOT24_IDX)", "SLOT32_MODULE=$(SLOT32_MODULE)", "SLOT32_IDX=$(SLOT32_IDX)", "SLOT33_MODULE=$(SLOT33_MODULE)", "SLOT33_IDX=$(SLOT33_IDX)", "SLOT48_MODULE=$(SLOT48_MODULE)", "SLOT48_IDX=$(SLOT48_IDX)", "SLOT49_MODULE=$(SLOT49_MODULE)", "SLOT49_IDX=$(SLOT49_IDX)") 

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic, MCH_ADDR=$(MCH_ADDR), ARCHIVER=$(ARCHIVER), ARCHIVER_SIZE=$(ARCHIVER_SIZE), P=$(P), CRATE_NUM=$(CRATE_NUM), TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

# essioc module
iocshLoad("$(essioc_DIR)common_config.iocsh")

iocInit()

eltc "$(DISP_MSG)"
