require ipmimanager,1.0.0
require essioc

epicsEnvSet("ENGINEER","Wojciech")
epicsEnvSet("LOCATION","DMCS")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")



# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 1)


# crate IP address
epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "10.1.3.97")
epicsEnvSet("TELNET_PORT", "23")

# 1=enable/0=disable archiver
epicsEnvSet("ARCHIVER", "1")
# archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

# panels compatibility version
epicsEnvSet("PANEL_VER", "1.0.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode 
epicsEnvSet("NAME_MODE", 1)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "ipmiManager/Db/auto-gen-db")

############################
# connect to specific MCH
############################

######################
# for NAME_MODE=1
######################

epicsEnvSet("P", "TD-D218:")

epicsEnvSet("MTCA_PREF", "$(P):")
epicsEnvSet("IOC_PREF", "$(P):")

epicsEnvSet("SLOT1_MODULE",  "CPU")
epicsEnvSet("SLOT1_IDX",     "1")
epicsEnvSet("SLOT2_MODULE",  "EVR")
epicsEnvSet("SLOT2_IDX",     "1")
epicsEnvSet("SLOT3_MODULE",  "AMC")
epicsEnvSet("SLOT3_IDX",     "1")
epicsEnvSet("SLOT5_MODULE",  "AMC")
epicsEnvSet("SLOT5_IDX",     "2")
epicsEnvSet("SLOT7_MODULE",  "AMC")
epicsEnvSet("SLOT7_IDX",     "3")
epicsEnvSet("SLOT19_MODULE", "RTM")
epicsEnvSet("SLOT19_IDX", "1")
epicsEnvSet("SLOT21_MODULE", "RTM")
epicsEnvSet("SLOT21_IDX", "2")
epicsEnvSet("SLOT23_MODULE", "RTM")
epicsEnvSet("SLOT23_IDX", "3")
epicsEnvSet("SLOT32_MODULE", "PM")
epicsEnvSet("SLOT32_IDX", "1")
epicsEnvSet("SLOT48_MODULE", "CU")
epicsEnvSet("SLOT48_IDX", "1")
epicsEnvSet("SLOT49_MODULE", "CU")
epicsEnvSet("SLOT49_IDX", "2")

epicsEnvSet("CHASSIS_CONFIG","SLOT1_MODULE=$(SLOT1_MODULE), SLOT1_IDX=$(SLOT1_IDX), SLOT2_MODULE=$(SLOT2_MODULE), SLOT2_IDX=$(SLOT2_IDX), SLOT3_MODULE=$(SLOT3_MODULE), SLOT3_IDX=$(SLOT3_IDX), SLOT5_MODULE=$(SLOT5_MODULE), SLOT5_IDX=$(SLOT5_IDX), SLOT7_MODULE=$(SLOT7_MODULE), SLOT7_IDX=$(SLOT7_IDX), SLOT19_MODULE=$(SLOT19_MODULE), SLOT19_IDX=$(SLOT19_IDX), SLOT21_MODULE=$(SLOT21_MODULE), SLOT21_IDX=$(SLOT21_IDX), SLOT23_MODULE=$(SLOT23_MODULE), SLOT23_IDX=$(SLOT23_IDX), SLOT32_MODULE=$(SLOT32_MODULE), SLOT32_IDX=$(SLOT32_IDX), SLOT48_MODULE=$(SLOT48_MODULE), SLOT48_IDX=$(SLOT48_IDX), SLOT49_MODULE=$(SLOT49_MODULE), SLOT49_IDX=$(SLOT49_IDX)")


iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic,MCH_ADDR=$(MCH_ADDR),ARCHIVER=$(ARCHIVER),ARCHIVER_SIZE=$(ARCHIVER_SIZE),P=$(P), CRATE_NUM=$(CRATE_NUM),TIMEOUT=10,USE_STREAM=,STREAM_PORT=$(TELNET_PORT),USE_EXPERT=,USE_ESSIOC=,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

# essioc module
iocshLoad("$(essioc_DIR)common_config.iocsh")

iocInit()

eltc "$(DISP_MSG)"
