require ipmimanager,1.0.0
#require essioc

epicsEnvSet("ENGINEER","Wojciech")
epicsEnvSet("LOCATION","DMCS")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")


epicsEnvSet("P", "LabS:")


# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 1)


# crate IP address
epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "192.168.1.144")
epicsEnvSet("TELNET_PORT", "23")

# 1=enable/0=disable archiver
epicsEnvSet("ARCHIVER", "1")
# archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

#Panels compatibility version
epicsEnvSet("PANEL_VER", "1.0.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode
epicsEnvSet("NAME_MODE", 0)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "icblm")

############################
# connect to specific MCH
############################
epicsEnvSet("MTCA_PREF", "$(P)-MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF", "$(P)-IOC-$(CRATE_NUM)00:")

epicsEnvSet("SLOT1_MODULE",  "CPU")
epicsEnvSet("SLOT1_IDX",     "01")
epicsEnvSet("SLOT2_MODULE",  "EVR")
epicsEnvSet("SLOT2_IDX",     "01")
epicsEnvSet("SLOT3_MODULE",  "AMC")
epicsEnvSet("SLOT3_IDX",     "10")
epicsEnvSet("SLOT5_MODULE",  "AMC")
epicsEnvSet("SLOT5_IDX",     "20")
epicsEnvSet("SLOT12_MODULE",  "AMC")
epicsEnvSet("SLOT12_IDX",     "30")
epicsEnvSet("SLOT28_MODULE", "RTM")

epicsEnvSet("CHASSIS_CONFIG","SLOT1_MODULE=$(SLOT1_MODULE), SLOT1_IDX=$(SLOT1_IDX), SLOT2_MODULE=$(SLOT2_MODULE), SLOT2_IDX=$(SLOT2_IDX), SLOT3_MODULE=$(SLOT3_MODULE), SLOT3_IDX=$(SLOT3_IDX), SLOT5_MODULE=$(SLOT5_MODULE), SLOT5_IDX=$(SLOT5_IDX), SLOT12_MODULE=$(SLOT12_MODULE), SLOT12_IDX=$(SLOT12_IDX), SLOT28_MODULE=$(SLOT28_MODULE)")

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=icBlm,MCH_ADDR=$(MCH_ADDR),ARCHIVER=$(ARCHIVER),ARCHIVER_SIZE=$(ARCHIVER_SIZE),P=$(P), CRATE_NUM=$(CRATE_NUM),TIMEOUT=10,USE_STREAM=,STREAM_PORT=$(TELNET_PORT),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

# essioc module
#iocshLoad("$(essioc_DIR)common_config.iocsh")

iocInit()

eltc "$(DISP_MSG)"
